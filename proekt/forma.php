<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="css/infostyle.css">


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title></title>
</head>
<body>

	<h1 class="text-center">Еден чекор ве дели од вашата веб страна</h1>

	<div class="container">
		<div class="row">
			<div class="col-md-offset-4 col-md-4 col-md-offset-4">
				<form action="insert.php" method="POST">
					<label>Напишете го линкот до cover сликата:</label>
					<br>
					<input type="text" name="cover">
					<br>
					<label>Внесете го насловот:</label>
					<br>
					<input type="text" name="naslov">
					<br>
					<label>Внесете го поднасловот:</label>
					<br>
					<input type="text" name="podnaslov">
					<br>
					<label>Напишете нешто за вас:</label>
					<br>
					<textarea rows="10" name="ZaVas"></textarea>
					<br>
					<label>Внесете го вашиот телефон:</label>
					<br>
					<input type="text" name="telefon">
					<br>
					<label>Внесете ја вашата локација:</label>
					<br>
					<input type="text" name="lokacija">
					<br>
					<label>Одберете дали нудите сервиси или продукти:</label>
					<br>
					<select name="selekcija">
						<option>Сервиси</option>
						<option>Продукти</option>
					</select>
					<br>
					<label>URL од слика</label>
					<br>
					<input type="text" name="slika1">
					<br>
					<label>Опис за сликата</label>	
					<br>
					<textarea rows="10" name="opis1"></textarea>
					<br>
					<label>URL од слика</label>
					<br>
					<input type="text" name="slika2">
					<br>
					<label>Опис за сликата</label>
					<br>
					<textarea rows="10" name="opis2"></textarea>
					<br>
					<label>URL од слика</label>
					<br>
					<input type="text" name="slika3">
					<br>
					<label>Опис за сликата</label>
					<br>
					<textarea rows="10" name="opis3"></textarea>
					<br>
					<label>Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве контактираат:</label>
					<br>
					<textarea rows="10" name="ZaFirmata"></textarea>
					<br>
					<label>Linkedin</label>
					<br>
					<input type="text" name="linkedin">
					<br>
					<label>Facebook</label>
					<br>
					<input type="text" name="facebook">
					<br>
					<label>Tweeter</label>
					<br>
					<input type="text" name="tweeter">
					<br>
					<label>Google+</label>
					<br>
					<input type="email" name="email">
					<br>
					<input type="submit" name="submit" value="Потврди">
				</form>
			</div>
		</div>
	</div>
</body>
</html>