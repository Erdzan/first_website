<?php

$username = 'root';
$password = 'root';
$database_type = 'mysql';
$database_host = 'localhost';
$database_name = 'WEB';



try
{
	$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);


$statement = $connection->prepare('INSERT INTO forma (cover, naslov, podnaslov, ZaVas, telefon, lokacija, selekcija, slika1, opis1, slika2, opis2, slika3, opis3, ZaFirmata, linkedin, facebook, tweeter, email) VALUES (:cover, :naslov, :podnaslov, :ZaVas, :telefon, :lokacija, :selekcija, :slika1, :opis1, :slika2, :opis2, :slika3, :opis3, :ZaFirmata, :linkedin, :facebook, :tweeter, :email)');

$statement->bindParam('cover', $_POST['cover'], PDO::PARAM_STR);
$statement->bindParam('naslov', $_POST['naslov'], PDO::PARAM_STR);
$statement->bindParam('podnaslov', $_POST['podnaslov'], PDO::PARAM_STR);
$statement->bindParam('ZaVas', $_POST['ZaVas'], PDO::PARAM_STR);
$statement->bindParam('telefon', $_POST['telefon'], PDO::PARAM_STR);
$statement->bindParam('lokacija', $_POST['lokacija'], PDO::PARAM_STR);
$statement->bindParam('selekcija', $_POST['selekcija'], PDO::PARAM_STR);
$statement->bindParam('slika1', $_POST['slika1'], PDO::PARAM_STR);
$statement->bindParam('opis1', $_POST['opis1'], PDO::PARAM_STR);
$statement->bindParam('slika2', $_POST['slika2'], PDO::PARAM_STR);
$statement->bindParam('opis2', $_POST['opis2'], PDO::PARAM_STR);
$statement->bindParam('slika3', $_POST['slika3'], PDO::PARAM_STR);
$statement->bindParam('opis3', $_POST['opis3'], PDO::PARAM_STR);
$statement->bindParam('ZaFirmata', $_POST['ZaFirmata'], PDO::PARAM_STR);
$statement->bindParam('linkedin', $_POST['linkedin'], PDO::PARAM_STR);
$statement->bindParam('facebook', $_POST['facebook'], PDO::PARAM_STR);
$statement->bindParam('tweeter', $_POST['tweeter'], PDO::PARAM_STR);
$statement->bindParam('email', $_POST['email'], PDO::PARAM_STR);


	$statement->execute();
	$lastId = $connection->lastInsertId();

	header('Location: web.php?id='.$lastId);
}
catch(PDOException $e)
{
   var_dump($e->getMessage());
}



?>