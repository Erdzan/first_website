<?php

$username = 'root';
$password = 'root';
$database_type = 'mysql';
$database_host = 'localhost';
$database_name = 'WEB';

$id = $_GET['id'];


try
{
	$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

	$statement = $connection->prepare('SELECT * FROM forma WHERE id = :id');

	$statement->bindParam(':id', $id);

	$statement->execute();

	$info = $statement->fetch(PDO::FETCH_ASSOC);

}
catch(PDOException $e)
{
	var_dump($e->getMessage());
}

?> 






<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="css/web.css">


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title></title>
</head>
<body>
	<div class="back1" id="doma">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 black">
					<ul class="list-unstyled list-inline pull-left">
						<li><a href="#doma">Дома</a></li>
						<li><a href="#Za Nas">За нас</a></li>
						<li><a href="#selekcija"><?php echo $info['selekcija'];?></a></li>
						<li><a href="#kontakt">Контакт</a></li>
					</ul>
				</div>
			</div>
	    </div>
	</div>

	<div class="pozadina" style="background-image: url(<?php echo $info['cover']?>)">
		<h1 class="text-center"><?php echo $info['naslov']?></h1>
		<h2 class="text-center"><?php echo $info['podnaslov']?></h2>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8" id="Za Nas">
				<h1>За Нас</h1>
				<p><?php echo $info['ZaVas'];?></p>
			</div>
			<div class="col-sm-offset-1 col-sm-2 col-sm-offset-1" id="kontakt">
				<h3>Телефон</h3>
				<p><?php echo $info['telefon'];?></p>
				<h3>Локација</h3>
				<p><?php echo $info['lokacija'];?></p>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row lead text-center tekst" id="selekcija">
			<p><?php echo $info['selekcija'];?></p>
		</div>
	</div>

	<div class="container">
		<div class="row sliki">
			<div class="col-sm-4">
				<img src="<?php echo $info['slika1'];?>">
				<p><?php echo $info['opis1'];?></p>
			</div>

			<div class="col-sm-4">
				<img src="<?php echo $info['slika2'];?>">
				<p><?php echo $info['opis2'];?></p>	
			</div>

			<div class="col-sm-4">
				<img src="<?php echo $info['slika3'];?>">
				<p><?php echo $info['opis3'];?></p>	
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row lead text-center">
			<h1>Контакт</h1>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-5">
				<h1>За Нас</h1>
				<p><?php echo $info['ZaFirmata'];?></p>
			</div>
			<div class="col-sm-offset-2 col-sm-5">
				<form>
					<label>Име</label>
					<br>
					<input type="text" name="ime" placeholder="Вашето име">
					<br>
					<label>Емаил</label>
					<br>
					<input type="email" name="email" placeholder="Вашиот емаил">
					<br>
					<label>Порака</label>
					<br>
					<textarea rows="5" placeholder="Вашата Порака"></textarea>
					<br>
	        	    <p class="text-center"><a href="" class="btn btn-info">Испрати</a></p>
				</form>
			</div>
		</div>
	</div>


	<div class="container-fluid footer">
		<div class="row">
			<div class="col-sm-8 ime">
				<p>Copyright by Erdzan S.-2018</p>
			</div>
			<div class="col-md-4 icon lead text-center">
				<a href="https://linkedin.com"><img src="images/linkedin.png"></a>
				<a href="https://facebook.com/"><img src="images/facebook.png"></a>
				<a href="https://twitter.com"><img src="images/twitter.png"></a>
				<a href="https://plus.google.com"><img src="images/google.png"></a>
			</div>
		</div>
	</div>


</body>
</html>



